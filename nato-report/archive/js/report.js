// *********************************************************************
//
// Global variables
//
// *********************************************************************

// var for total number of attendees using the system. For analytics
var _totalUsers = 0;

// var for tabular list of the users and their lunch answers. For analytics
var _lunchList = null;

// var for the complete list of users and their icon/avatar selection
var _registrationList = null;

// var used to keep track of current new index value when filling lunch list
var _currentId = 0;

// vars for html for all lunch choices, top 5 and bottom 5 calorie intakers
var _top5Html = "";
var _bottom5Html = "";
var _lunchListHtml = "";

// vars for lunch data back from LRS. Global scope used for cross stmt list calulations
var _day1BreadStmts = null;
var _day1SandwichStmts = null;
var _day1ChipsStmts = null;
var _day1DrinkStmts = null;
var _day2BreadStmts = null;
var _day2SandwichStmts = null;
var _day2ChipsStmts = null;
var _day2DrinkStmts = null;

// *********************************************************************
//
// Init methods for each report: Graphs and Analytics
//
// *********************************************************************

// Initialize Method for Analytics page.  Called onload
function initAnalytics()
{
    // Initialize the questions.  In a 'real' system this would come from an outside system or LRS
    initApp();

    ShowTotalUsers();
    ShowLunchAnalytics();
}

// Initialize Method for Graphs page. Called onload
function initGraphs()
{
    // Initialize the questions.  In a 'real' system this would come from an outside system or LRS
    initApp();

    CreateQuestion1_1Report();
    CreateQuestion1_2Report();
    CreateQuestion1_3Report();
    CreateQuestion1_4Report();
}

// *********************************************************************
//
// ANALYTICS - Data functions
//
// *********************************************************************

// Gets all lunch choices, calculates calories, then shows lists
function ShowLunchAnalytics()
{
    // init global
    _lunchList = new Array();

    // Setup queries and get data from Lrs to globals
    GetLunchDataFromLrs();

    // Fill up tabular data structure with the 8 questions
    PopulateQuestionData(_day1BreadStmts, "day1BreadChoice");
    PopulateQuestionData(_day1SandwichStmts, "day1SandwichChoice");
    PopulateQuestionData(_day1ChipsStmts, "day1ChipsChoice");
    PopulateQuestionData(_day1DrinkStmts, "day1DrinkChoice");
    PopulateQuestionData(_day2BreadStmts, "day2BreadChoice");
    PopulateQuestionData(_day2SandwichStmts, "day2SandwichChoice");
    PopulateQuestionData(_day2ChipsStmts, "day2ChipsChoice");
    PopulateQuestionData(_day2DrinkStmts, "day2DrinkChoice");

    // Calculate calories based on users choices
    CalculateCalories();

    // Show raw data and top 5s
    ShowLunchData();
    ShowTop5s();
}

// Setup search parameters for each lunch question, query LRS and set global stmts
function GetLunchDataFromLrs()
{
    // Setup up search arg objects
    var day1BreadSearch = ADL.XAPIWrapper.searchParams();
    var day1SandwichSearch = ADL.XAPIWrapper.searchParams();
    var day1ChipsSearch = ADL.XAPIWrapper.searchParams();
    var day1DrinkSearch =ADL.XAPIWrapper.searchParams();
    var day2BreadSearch = ADL.XAPIWrapper.searchParams();
    var day2SandwichSearch = ADL.XAPIWrapper.searchParams();
    var day2ChipsSearch = ADL.XAPIWrapper.searchParams();
    var day2DrinkSearch =ADL.XAPIWrapper.searchParams();

    // Configure activity id as search
    day1BreadSearch['activity'] = q1_1.id;
    day1SandwichSearch['activity'] = q1_2.id;
    day1ChipsSearch['activity'] = q1_3.id;
    day1DrinkSearch['activity'] = q1_4.id;
    day2BreadSearch['activity'] = q7_1.id;
    day2SandwichSearch['activity'] = q7_2.id;
    day2ChipsSearch['activity'] = q7_3.id;
    day2DrinkSearch['activity'] = q7_4.id;

    // Get question data from LRS
    _day1BreadStmts = GetCompleteStatementListFromLRS(day1BreadSearch);
    _day1SandwichStmts = GetCompleteStatementListFromLRS(day1SandwichSearch);
    _day1ChipsStmts = GetCompleteStatementListFromLRS(day1ChipsSearch);
    _day1DrinkStmts = GetCompleteStatementListFromLRS(day1DrinkSearch);
    _day2BreadStmts = GetCompleteStatementListFromLRS(day2BreadSearch);
    _day2SandwichStmts = GetCompleteStatementListFromLRS(day2SandwichSearch);
    _day2ChipsStmts = GetCompleteStatementListFromLRS(day2ChipsSearch);
    _day2DrinkStmts = GetCompleteStatementListFromLRS(day2DrinkSearch);
}

// Use global lunch stmts and populate tabular data structure (also global scope - _lunchList)
function PopulateQuestionData(statements, property)
{
    // fill up sandwich day 1 
    for(var i=0; i < statements.length; i++)
    {
        if(statements[i].result != undefined && statements[i].actor.account.name != undefined)
        {
            // create row var
            var row = null;

            // check to see if user already exists,  if not initialize
            var u = GetRowIndex(statements[i].actor.account.name);

            if (u === -1)
            {
                // new user so create new object
                row = new Object();
                row.name = statements[i].actor.account.name;
                row[property] = statements[i].result.response;

                // add to global list, increment current counter
                _lunchList[_currentId] = row;
                _currentId++;
            }
            else
            {
                // user already exists
                _lunchList[u][property] = statements[i].result.response;
            }
        }
    }

}

// *********************************************************************
//
// ANALYTICS - Calculations
//
// *********************************************************************

// Uses tabular data structure (_lunchList) to calculate total calories
function CalculateCalories()
{
    for (var i=0; i<_lunchList.length; i++)
    {
        // figure out if they answered all questions
        missedAnAnswer = false;

        // day 1
        _lunchList[i].calories = 0;

        // day 1 bread
        var day1BreadLetter = _lunchList[i].day1BreadChoice;
        if (day1BreadLetter === undefined)
        {
            day1BreadLetter = "x";  
            missedAnAnswer = true;
        }
        _lunchList[i].calories += q1_1[day1BreadLetter.toLowerCase() + CALORIE_SUFFIX];
        
        // day 1 sandwich
        var day1SandwichLetter = _lunchList[i].day1SandwichChoice;
        if (day1SandwichLetter === undefined)
        {
            day1SandwichLetter = "x";    
            missedAnAnswer = true;           
        }
        _lunchList[i].calories += q1_2[day1SandwichLetter.toLowerCase() + CALORIE_SUFFIX];

        // day 1 chips - 1 or 0 must use if stmt
        var day1ChipsLetter = _lunchList[i].day1ChipsChoice;
        if (day1ChipsLetter === undefined)
        {
            day1ChipsLetter = "x";     
            missedAnAnswer = true;        
        }
        if (_lunchList[i].day1ChipsChoice === 1)
        {
            _lunchList[i].calories += q1_3.a_cal;
        }
        
        // day 1 drink
        var day1DrinkLetter = _lunchList[i].day1DrinkChoice;
        if (day1DrinkLetter === undefined)
        {
             day1DrinkLetter = "x";      
             missedAnAnswer = true;        
        }
        _lunchList[i].calories += q1_4[day1DrinkLetter.toLowerCase() + CALORIE_SUFFIX];

        // day 2        
        // day 2 bread
        var day2BreadLetter = _lunchList[i].day2BreadChoice;
        if (day2BreadLetter === undefined)
        {
             day2BreadLetter = "x";     
             missedAnAnswer = true;        
        }
        _lunchList[i].calories += q7_1[day2BreadLetter.toLowerCase() + CALORIE_SUFFIX];
        
        // day 2 sandwich
        var day2SandwichLetter = _lunchList[i].day2SandwichChoice;
        if (day2SandwichLetter === undefined)
        {
             day2SandwichLetter = "x";   
             missedAnAnswer = true;          
        }
        _lunchList[i].calories += q7_2[day2SandwichLetter.toLowerCase() + CALORIE_SUFFIX];

        // day 2 chips - 1 or 0 must use if stmt
        var day2ChipsLetter = _lunchList[i].day2ChipsChoice;
        if (day2ChipsLetter === undefined)
        {
            day2ChipsLetter = "x";  
            missedAnAnswer = true;          
        }
        if (_lunchList[i].day2ChipsChoice === 1)
        {
            _lunchList[i].calories += q7_3.a_cal;
        }
        
        // day 2 drink
        var day2DrinkLetter = _lunchList[i].day2DrinkChoice;
        if (day2DrinkLetter === undefined)
        {
            day2DrinkLetter = "x";     
            missedAnAnswer = true;        
        }
        
        // set calories        
        _lunchList[i].calories += q7_4[day2DrinkLetter.toLowerCase() + CALORIE_SUFFIX];

        // set whether or not they missed a question
        _lunchList[i].missedAnAnswer = missedAnAnswer;
    }
}

// Setup registration global list, calculate total users and display
function ShowTotalUsers()
{
     // init global
    _registrationList = new Array();

    var search = ADL.XAPIWrapper.searchParams();
    search['activity'] = q0.id;
    search['verb'] = ADL.verbs.registered.id;

    var statements = GetCompleteStatementListFromLRS(search);
 
    for (var i=0; i < statements.length; i++)
    {
        _totalUsers++;

        // fill the icon list for later use
        if (statements[i].result != undefined)
        {
            var usr = new Object();
            usr.name = statements[i].actor.account.name;
            usr.icon = statements[i].result.response;
            _registrationList[i] = usr;
        }
    }        

    $('#TotalUsers').html(_totalUsers); 
}

// *********************************************************************
//
// ANALYTICS - Show Html
//
// *********************************************************************

// Create and populate HTML variables for best and worst lunch choices (by rough calories estimates)
function ShowTop5s()
{
    if (_lunchList.length > 0)
    {
        _lunchList.sort(CompareCalories);

        // Lowest 5
        _bottom5Html = "<table><thead><tr><th>&nbsp;</th><th>Name</th><th>Calories</th></tr><thead><tbody>";
        for (var i=0; i < 5; i++)
        {
            if(_lunchList[i] != undefined)
            {   
                // var to update based on whether or not they missed an answer
                var tdCode = "";

                if(_lunchList[i].missedAnAnswer)
                {
                    tdCode = "<td style='font-style:italic'>*";
                }
                else
                {
                    tdCode = "<td>";
                }
                _bottom5Html += "<tr>";
                _bottom5Html += "<td><img src='" + GetIcon(_lunchList[i].name) + "' /></td>";
                _bottom5Html += tdCode + _lunchList[i].name + "</td>";
                _bottom5Html += tdCode + _lunchList[i].calories + "</td>";
                _bottom5Html += "</tr>";                
            }
        }
        _bottom5Html += "</tbody></table>";

        // Highest 5
        _top5Html = "<table><thead><tr><th>&nbsp;</th><th>Name</th><th>Calories</th></tr><thead><tbody>";
        for (var i=_lunchList.length - 1; i > _lunchList.length - 6; i--)
        {
            if(_lunchList[i] != undefined)
            {
                // var to update based on whether or not they missed an answer
                var tdCode = "";
                
                if(_lunchList[i].missedAnAnswer)
                {
                    tdCode = "<td style='font-style:italic'>*";
                }
                else
                {
                    tdCode = "<td>";
                }                                
                
                _top5Html += "<tr>";
                _top5Html += "<td><img src='" + GetIcon(_lunchList[i].name) + "' /></td>";
                _top5Html += tdCode + _lunchList[i].name + "</td>";
                _top5Html += tdCode + _lunchList[i].calories + "</td>";
                _top5Html += "</tr>";
            }
        }
        _top5Html += "</tbody></table>";

        $('#Bottom5List').html(_bottom5Html); 
        $('#Top5List').html(_top5Html);  
    } 
}

// Create and populate HTML variables for users and their lunch choices
function ShowLunchData()
{
    _lunchList.sort(CompareNames);

    _lunchListHtml = "<table><thead><tr><th>&nbsp;</th><th>Name</th><th>Bread Day 1</th><th>Sandwich Day 1</th><th>Chips Day 1</th><th>Drink Day 1</th><th>Bread Day 2</th><th>Sandwich Day 2</th><th>Chips Day 2</th><th>Drink Day 2</th><th>Calories</th></tr></thead><tbody>"
    for (var i=0; i < _lunchList.length; i++)
    {
        var name = _lunchList[i].name;
        var day1_bread = _lunchList[i].day1BreadChoice;
        var day1_sandwich = _lunchList[i].day1SandwichChoice;
        var day1_chips = _lunchList[i].day1ChipsChoice;
        var day1_drink = _lunchList[i].day1DrinkChoice;
        var day2_bread = _lunchList[i].day2BreadChoice;
        var day2_sandwich = _lunchList[i].day2SandwichChoice;
        var day2_chips = _lunchList[i].day2ChipsChoice;
        var day2_drink = _lunchList[i].day2DrinkChoice;
        var calories = _lunchList[i].calories;

        // icon
        _lunchListHtml += "<tr><td><img src='" + GetIcon(name) + "' /></td>";

        // name
        _lunchListHtml += "<td>" + name + "</td>";

        // day 1 bread
        if (day1_bread != undefined)
        {
            _lunchListHtml += "<td>" + q1_1[day1_bread.toLowerCase()] + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Answered</td>";
        }

        // day 1 sandwich
        if (day1_sandwich != undefined)
        {
            _lunchListHtml += "<td>" + q1_2[day1_sandwich.toLowerCase()] + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Answered</td>";
        }

        // day 1 chips
        if (day1_chips != undefined)
        {
            var attr = "";
            if (day1_chips === "0")
                attr = "b";
            else
                attr = "a";

            _lunchListHtml += "<td>" + q1_3[attr] + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Answered</td>";
        }

        // day 1 drink
        if (day1_drink != undefined)
        {
            _lunchListHtml += "<td>" + q1_4[day1_drink.toLowerCase()] + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Answered</td>";
        }

        // day 2 bread
        if (day2_bread != undefined)
        {
            _lunchListHtml += "<td>" + q7_1[day2_bread.toLowerCase()] + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Answered</td>";
        }

        // day 2 sandwich
        if (day2_sandwich != undefined)
        {
            _lunchListHtml += "<td>" + q7_2[day2_sandwich.toLowerCase()] + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Answered</td>";
        }

        // day 2 chips
        if (day2_chips != undefined)
        {
            var attr = "";
            if (day2_chips === "0")
                attr = "b";
            else
                attr = "a";

            _lunchListHtml += "<td>" + q7_3[attr] + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Answered</td>";
        }

        // day 2 drink
        if (day2_drink != undefined)
        {
            _lunchListHtml += "<td>" + q7_4[day2_drink.toLowerCase()] + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Answered</td>";
        }

                // calories
        if (calories != undefined)
        {
            _lunchListHtml += "<td>" + calories + "</td>";
        } 
        else
        {
            _lunchListHtml += "<td>Not Calculated</td>";
        }

        _lunchListHtml += "</tr>";

    }

    _lunchListHtml += "</tbody></table>";

    $('#LunchList').html(_lunchListHtml); 
}

// *********************************************************************
//
// GRAPHS - Methods for the individual questions
//
// *********************************************************************

// Get data, count results and render graphs for queston 1_1
function CreateQuestion1_1Report()
{
    // Question 1_1
    var search = ADL.XAPIWrapper.searchParams();
    search['activity'] = q1_1.id;
    var statements = GetCompleteStatementListFromLRS(search);

    var aCount = 0;
    var bCount = 0;

    var i = statements.length;
    
    for (var i=0; i < statements.length; i++)
    {
        if(statements[i].result != undefined)
        {
            switch(statements[i].result.response)
            {
                case "A":
                    aCount++;
                    break;
                case "B":
                    bCount++;
                    break;
                default:
                    // invalid choice got in the LRS
                    console.log("Data Validation Error in 'CreateQuestion1_1Report()'");
                    console.log("     - Invalid Case: " + statements[i].result.response);
                    console.log("     - Actor Name: " + statements[i].actor.account.name);         
            }
        }
    }

    var pieData = [
        { label: q1_1.a, data: aCount},
        { label: q1_1.b, data: bCount}
    ];

    var barData = [[1,aCount], [2, bCount]]; 
    var barTicks = [[1, q1_1.a], [2, q1_1.b]];

    // display
    $('#q1_1').html(q1_1.text);
    ShowPieChart(pieData, "#q1_1PieGraph");
    ShowBarGraph(barData, barTicks, "#q1_1BarGraph");
}

// Get data, count results and render graphs for queston 1_2
function CreateQuestion1_2Report()
{
    // Question 1_2
    var search = ADL.XAPIWrapper.searchParams();
    search['activity'] = q1_2.id;
    var statements = GetCompleteStatementListFromLRS(search);

    var aCount = 0;
    var bCount = 0;
    var cCount = 0;
    var dCount = 0;
    var eCount = 0;
    var fCount = 0;
    var gCount = 0;
    var bCount = 0;
    var hCount = 0;
    var iCount = 0;
    var jCount = 0;
    
    var i = statements.length;
    while(i--)
    {
        if (statements[i].result != undefined)
        {
            switch(statements[i].result.response)
            {
                case "A":
                    aCount++;
                    break;
                case "B":
                    bCount++;
                    break;
                case "C":
                    cCount++;
                    break;
                case "D":
                    dCount++;
                    break;
                case "E":
                    eCount++;
                    break;
                case "F":
                    fCount++;
                    break;
                case "G":
                    gCount++;
                    break;
                case "H":
                    hCount++;
                    break;
                case "I":
                    iCount++;
                    break;
                case "J":
                    jCount++;
                    break;
                default:
                    // invalid choice got in the LRS
                    console.log("Data Validation Error in 'CreateQuestion1_2Report()'");
                    console.log("     - Invalid Case: " + statements[i].result.response);
                    console.log("     - Actor Name: " + statements[i].actor.account.name);      
            }            
        }
    }

   var pieData = [
        { label: q1_2.a, data: aCount},
        { label: q1_2.b, data: bCount},
        { label: q1_2.c, data: cCount},
        { label: q1_2.d, data: dCount},
        { label: q1_2.e, data: eCount},
        { label: q1_2.f, data: fCount},
        { label: q1_2.g, data: gCount},
        { label: q1_2.h, data: hCount},
        { label: q1_2.i, data: iCount},
        { label: q1_2.j, data: jCount}
    ];

    var barData = [
        [1, aCount], 
        [2, bCount],
        [3, cCount], 
        [4, dCount],
        [5, eCount], 
        [6, fCount],
        [7, gCount], 
        [8, hCount],
        [9, iCount], 
        [10, jCount]    
    ]; 
    var barTicks = [
        [1, q1_2.a], 
        [2, q1_2.b],
        [3, q1_2.c], 
        [4, q1_2.d],
        [5, q1_2.e], 
        [6, q1_2.f],
        [7, q1_2.g], 
        [8, q1_2.h],
        [9, q1_2.i], 
        [10, q1_2.j]
    ];

    // display
    $('#q1_2').html(q1_2.text);
    ShowPieChart(pieData, "#q1_2PieGraph");
    ShowBarGraph(barData, barTicks, "#q1_2BarGraph");
    
}

// Get data, count results and render graphs for queston 1_3
function CreateQuestion1_3Report()
{   
    // Question 1_3
    var search = ADL.XAPIWrapper.searchParams();
    search['activity'] = q1_3.id;
    var statements = GetCompleteStatementListFromLRS(search);

    var aCount = 0;
    var bCount = 0;
    
    var i = statements.length;
    while(i--)
    {
        if (statements[i].result != undefined)
        {
            switch(statements[i].result.response)
            {
                case "1":
                    aCount++;
                    break;
                case "0":
                    bCount++;
                    break;
                default:
                    // invalid choice got in the LRS
                    console.log("Data Validation Error in 'CreateQuestion1_3Report()'");
                    console.log("     - Invalid Case: " + statements[i].result.response);
                    console.log("     - Actor Name: " + statements[i].actor.account.name);         
            }  
        }
    }

   var pieData = [
        { label: q1_3.a, data: aCount},
        { label: q1_3.b, data: bCount}
    ];

    var barData = [
        [1, aCount], 
        [2, bCount]    
    ]; 
    var barTicks = [
        [1, q1_3.a], 
        [2, q1_3.b]
    ];

    // display
    $('#q1_3').html(q1_3.text);
    ShowPieChart(pieData, "#q1_3PieGraph");
    ShowBarGraph(barData, barTicks, "#q1_3BarGraph");    
}

// Get data, count results and render graphs for queston 1_4
function CreateQuestion1_4Report()
{
    // Question 1_4
    var search = ADL.XAPIWrapper.searchParams();
    search['activity'] = q1_4.id;
    var statements = GetCompleteStatementListFromLRS(search);

    var aCount = 0;
    var bCount = 0;
    var cCount = 0;
    var dCount = 0;
    var eCount = 0;
    
    var i = statements.length;
    while(i--)
    {
        if (statements[i].result != undefined)
        {
            switch(statements[i].result.response)
            {
                case "A":
                    aCount++;
                    break;
                case "B":
                    bCount++;
                    break;
                case "C":
                    cCount++;
                    break;
                case "D":
                    dCount++;
                    break;
                case "E":
                    eCount++;
                    break;
                default:
                    // invalid choice got in the LRS
                    console.log("Data Validation Error in 'CreateQuestion1_4Report()'");
                    console.log("     - Invalid Case: " + statements[i].result.response);
                    console.log("     - Actor Name: " + statements[i].actor.account.name);        
            }
        }
    }

   var pieData = [
        { label: q1_4.a, data: aCount},
        { label: q1_4.b, data: bCount},
        { label: q1_4.c, data: cCount},
        { label: q1_4.d, data: dCount},
        { label: q1_4.e, data: eCount}
    ];

    var barData = [
        [1, aCount], 
        [2, bCount],
        [3, cCount], 
        [4, dCount],
        [5, eCount]    
    ]; 
    var barTicks = [
        [1, q1_4.a], 
        [2, q1_4.b],
        [3, q1_4.c], 
        [4, q1_4.d],
        [5, q1_4.e]
    ];

    // display
    $('#q1_4').html(q1_4.text);
    ShowPieChart(pieData, "#q1_4PieGraph");
    ShowBarGraph(barData, barTicks, "#q1_4BarGraph");    
}

// *********************************************************************
//
// GRAPHS - Functions to help display
//
// *********************************************************************

// Helper method that shows bar graph
function ShowBarGraph(barData, barTicks, div)
{
    var stack = 0, bars = true, lines = false, steps = false;

    $.plot(
       $(div),
       [
        {
          data: barData,
          bars: {
            show: true,
            barWidth: 0.5,
            align: "center"
          }   
        }
     ],
     {
       xaxis: {
         ticks: barTicks
       }   
     }
    );
}

// Helper method that shows pie chart
function ShowPieChart(data, div)
{
    $.plot($(div), data,
    {
        series: {
            pie: { 
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 3/4,
                    formatter: function(label, series){
                        return '<div style="font-size:.8em;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
                    },
                    background: { 
                        opacity: 0.5,
                        color: '#000'
                    }
                }
            }
        },
        legend: {
            show: false
        }
    });
}

// *********************************************************************
//
// ANALYTICS - Util
//
// *********************************************************************

// Uses XAPIWrapper to get statements, loop through "more" until all results
// are returned for the specified search arguments.
function GetCompleteStatementListFromLRS(search)
{
    var result = ADL.XAPIWrapper.getStatements(search);
    var statements = result.statements;

    while(result.more && result.more !== "")
    {
        var res = ADL.XAPIWrapper.getStatements(null, result.more);
        var stmts = res.statements;

        statements.push.apply(statements, stmts);

        result = res;
    }   

    return statements;
}

// Compares an object's calories member.  For sorting.
function CompareCalories(a, b)
{
  if (a.calories < b.calories)
     return -1;
  if (a.calories > b.calories)
    return 1;
  return 0;

}

// Compares an object's name member.  For sorting.
function CompareNames(a, b)
{
  if (a.name.toLowerCase() < b.name.toLowerCase())
     return -1;
  if (a.name.toLowerCase() > b.name.toLowerCase())
    return 1;
  return 0;    
}

// Gets row index based on users name
function GetRowIndex(name)
{
    var index = -1;

    for (var i = 0; i < _lunchList.length; i++)
    {    
        if (_lunchList[i].name === name)
        {
            index = i;
            break;
        }
    }

    return index;
}

// Gets icon URL based on users name
function GetIcon(name)    
{
    var icon = q0.a;

    for (var i = 0; i < _registrationList.length; i++)
    {    
        if(_registrationList[i] != undefined)
        {
            if (_registrationList[i].name === name)
            {
                icon = q0[_registrationList[i].icon.toLowerCase()];
                break;
            }            
        }

    }

    return icon;
}