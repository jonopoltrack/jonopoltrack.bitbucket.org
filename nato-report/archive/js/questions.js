

// TODO - Remove "" before deploy

// question objects using <session>_<number>
var q0 = null;
var q1_1 = null;
var q1_2 = null;
var q1_3 = null;
var q1_4 = null;
var q2_1 = null;
var q3_1 = null;
var q3_2 = null;
var q3_3 = null;
var q4_1 = null;
var q5_1 = null;
var q5_2 = null;
var q6_1 = null;
var q6_2 = null;
var q7_1 = null;
var q7_2 = null;
var q7_3 = null;
var q7_4 = null;
var q7_5 = null;
var q8_1 = null;
var q9_1 = null;
var q9_2 = null;
var q11_1 = null;
var q_x = null;

var CALORIE_SUFFIX = "_cal";

// Initialize question data
function initApp()
{
    q0 = new Object();
    q1_1 = new Object();
    q1_2 = new Object();
    q1_3 = new Object();
    q1_4 = new Object();
    q2_1 = new Object();
    q3_1 = new Object();
    q3_2 = new Object();
    q3_3 = new Object();
    q4_1 = new Object();
    q5_1 = new Object();
    q5_2 = new Object();
    q6_1 = new Object();
    q6_2 = new Object();
    q7_1 = new Object();
    q7_2 = new Object();
    q7_3 = new Object();
    q7_4 = new Object();
    q7_5 = new Object();
    q8_1 = new Object();
    q9_1 = new Object();
    q9_2 = new Object();
    q11_1 = new Object();
    q_x = new Object();


    // Registration
    //****************************************************
    q0.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/0";
    q0.text = "Icon Selected";
    q0.a = "./img/userIcon1.png";
    q0.b = "./img/userIcon2.png";
    q0.c = "./img/userIcon3.png";
    q0.d = "./img/userIcon4.png";
    q0.e = "./img/userIcon5.png";
    q0.f = "./img/userIcon6.png";
    q0.g = "./img/userIcon7.png";
    q0.h = "./img/userIcon8.png";
    q0.i = "./img/userIcon9.png";
    q0.j = "./img/userIcon10.png";
    q0.k = "./img/userIcon11.png";
    q0.l = "./img/userIcon12.png";
    q0.m = "./img/userIcon13.png";
    q0.n = "./img/userIcon14.png";
    q0.o = "./img/userIcon15.png";
    q0.p = "./img/userIcon16.png";
    q0.q = "./img/userIcon17.png";
    q0.r = "./img/userIcon18.png";
    q0.s = "./img/userIcon19.png";
    q0.t = "./img/userIcon20.png";
    q0.u = "./img/userIcon21.png";
    q0.v = "./img/userIcon22.png";
    q0.w = "./img/userIcon23.png";
    q0.x = "./img/userIcon24.png";
    q0.y = "./img/userIcon25.png";
    q0.z = "./img/userIcon26.png";
    q0.aa = "./img/userIcon27.png";
    q0.bb = "./img/userIcon28.png";
    q0.cc = "./img/userIcon29.png";
    q0.dd = "./img/userIcon30.png";
    q0.ee = "./img/userIcon31.png";
    q0.ff = "./img/userIcon32.png";
    q0.gg = "./img/userIcon33.png";
    q0.hh = "./img/userIcon34.png";
    q0.ii = "./img/userIcon35.png";
    q0.jj = "./img/userIcon36.png";
    q0.kk = "./img/userIcon37.png";
    q0.ll = "./img/userIcon38.png";
    q0.mm = "./img/userIcon39.png";
    q0.nn = "./img/userIcon40.png";
    q0.oo = "./img/userIcon41.png";
    q0.pp = "./img/userIcon42.png";
    q0.qq = "./img/userIcon43.png";
    q0.rr = "./img/userIcon44.png";

    // Session 1
    //****************************************************
    // Question 1_1 
    q1_1.text = "What type of bread would you like your sandwich on for lunch (day 1)?";
    q1_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/1/question/1";
    q1_1.a = "White";
    q1_1.b = "Wheat";
    q1_1.a_cal = 50;
    q1_1.b_cal = 0;
    q1_1.x_cal = 0;

    // Question 1_2
    q1_2.text = "What type of Potbellys sandwich do you want (day 1)?";
    q1_2.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/1/question/2";
    q1_2.a = "Turkey Breast";
    q1_2.b = "A Wreck";
    q1_2.c = "Italian";
    q1_2.d = "Roast Beef";
    q1_2.e = "Meatball";
    q1_2.f = "Chicken Salad";
    q1_2.g = "Smoked Ham";
    q1_2.h = "Tuna Salad";
    q1_2.i = "Vegetarian";
    q1_2.j = "Grilled Chicken and Cheddar";
    q1_2.a_cal = 442;
    q1_2.b_cal = 538;
    q1_2.c_cal = 657;
    q1_2.d_cal = 512;
    q1_2.e_cal = 673;
    q1_2.f_cal = 591;
    q1_2.g_cal = 499;
    q1_2.h_cal = 549;
    q1_2.i_cal = 522;
    q1_2.j_cal = 564;
    q1_2.x_cal = 0;

    // Question 1_3
    q1_3.text = "Would you like chips with your sandwich (day 1)?";
    q1_3.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/1/question/3";
    q1_3.a = "Yes";
    q1_3.b = "No";
    q1_3.a_cal = 200;
    q1_3.b_cal = 0;
    q1_3.x_cal = 0;

    // Question 1_4
    q1_4.text = "What type of drink will you enjoy with lunch (day 1)?";
    q1_4.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/1/question/4";
    q1_4.a = "Soda";
    q1_4.b = "Diet Soda";
    q1_4.c = "Water";
    q1_4.d = "Coffee";
    q1_4.e = "None";
    q1_4.a_cal = 150;
    q1_4.b_cal = 30;
    q1_4.c_cal = 0;
    q1_4.d_cal = 30;
    q1_4.e_cal = 0;
    q1_4.x_cal = 0;
    
    // Session 2
    //****************************************************
    // Question 2_1 
    q2_1.text = "Rate this project on your interest level";
    q2_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/2/question/1";
    q2_1.a = "Wasn’t my favorite";
    q2_1.b = "Mildly interested";
    q2_1.c = "Is something I may build or buy";
    q2_1.d = "Is something I would likely build or buy";
    q2_1.e = "Gimme it now!";

    // Session 3
    //****************************************************
    // Question 3_1 
    q3_1.text = "What is your favorite pronunciation of xAPI?";
    q3_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/3/question/1";
    q3_1.a = "zappy";
    q3_1.b = "ex hay pee eye";
    q3_1.c = "ex happy";
    q3_1.d = "za pee";

    // Question 3_2
    q3_2.text = "How far did you travel to get here?";
    q3_2.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/3/question/2";
    q3_2.a = "Under 50 miles";
    q3_2.b = "50-200 miles";
    q3_2.c = "200-500 miles";
    q3_2.d = "500-1000 miles";
    q3_2.e = "1000+ miles";
 
    // Question 3_3
    q3_3.text = "What is the best feature of xAPI?";
    q3_3.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/3/question/3";

    // Session 4 (Repeat 1 - n)
    //****************************************************
    // Question 4_1
    q4_1.text = "This project...";
    q4_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/4/question/1";
    q4_1.a = "Wasn’t my favorite";
    q4_1.b = "Mildly interested";
    q4_1.c = "Is something I may build or buy";
    q4_1.d = "Is something I would likely build or buy";
    q4_1.e = "Gimme it now!";
    
    // Session 5
    //****************************************************
    // Question 5_1
    q5_1.text = "What is your favorite interesting pizza topping?";
    q5_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/5/question/1";

    // Question 5_2
    q5_2.text = "Which of the following interests you most in regards to tracking using xAPI?";
    q5_2.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/5/question/2";
    q5_2.a = "Simulation/Games";
    q5_2.b = "Mobile Learning";
    q5_2.c = "Offline/Disconnected Learning";
    q5_2.d = "Team Learning";
    q5_2.e = "Other";

    // Session 6
    //****************************************************
    // Question 6_1
    q6_1.text = "How much trust do you have in Social Networking sites?";
    q6_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/6/question/1";
    q6_1.a = "I keep a completely open profile";
    q6_1.b = "I share photos/status with friends and family";
    q6_1.c = "I use one, but I don’t really share much with anyone";
    q6_1.d = "I don’t use them for security reasons";
    q6_1.e = "I don’t use them for reasons other than security";

    // Question 6_2
    q6_2.text = "What is your favorite color of elephant?";
    q6_2.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/6/question/2";
    q6_2.a = "Pink (Yes, only one choice)";

    // Session 7
    //****************************************************
    // Question 7_1
    q7_1.text = "What type of bread would you like your sandwich on for lunch (day 2)?";
    q7_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/7/question/1";
    q7_1.a = "White";
    q7_1.b = "Wheat";
    q7_1.a_cal = 50;
    q7_1.b_cal = 0;
    q7_1.x_cal = 0;

    // Question 7_2
    q7_2.text = "What type of Potbelly’s sandwich do you want (day 2)?";
    q7_2.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/7/question/2";
    q7_2.a = "Turkey Breast";
    q7_2.b = "A Wreck";
    q7_2.c = "Italian";
    q7_2.d = "Roast Beef";
    q7_2.e = "Meatball";
    q7_2.f = "Chicken Salad";
    q7_2.g = "Smoked Ham";
    q7_2.h = "Tuna Salad";
    q7_2.i = "Vegetarian";
    q7_2.j = "Grilled Chicken and Cheddar";
    q7_2.a_cal = 442;
    q7_2.b_cal = 538;
    q7_2.c_cal = 657;
    q7_2.d_cal = 512;
    q7_2.e_cal = 673;
    q7_2.f_cal = 591;
    q7_2.g_cal = 499;
    q7_2.h_cal = 549;
    q7_2.i_cal = 522;
    q7_2.j_cal = 564;
    q7_2.x_cal = 0;

    // Question 7_3
    q7_3.text = "Would you like chips with your sandwich (day 2)?";
    q7_3.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/7/question/3";
    q7_3.a = "Yes";
    q7_3.b = "No";
    q7_3.a_cal = 200;
    q7_3.b_cal = 0;
    q7_3.x_cal = 0;

    // Question 7_4
    q7_4.text = "What type of drink will you enjoy with lunch (day 2)?";
    q7_4.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/7/question/4";
    q7_4.a = "Soda";
    q7_4.b = "Diet Soda";
    q7_4.c = "Water";
    q7_4.d = "Coffee";
    q7_4.e = "None";
    q7_4.a_cal = 150;
    q7_4.b_cal = 30;
    q7_4.c_cal = 0;
    q7_4.d_cal = 30;
    q7_4.e_cal = 0;
    q7_4.x_cal = 0;

    // Question 7_5
    q7_5.text = "What is your biggest worry about xAPI?";
    q7_5.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/7/question/5";

    // Session 8
    //****************************************************
    // Question 8_1
    q8_1.text = "Which do you believe more about xAPI?";
    q8_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/8/question/1";
    q8_1.a = "It is basically SCORM modernized to current technologies, which makes it more useful";
    q8_1.b = "It is completely unlike SCORM, which makes it more useful";
    q8_1.c = "I’d rather be SCORMing";

    // Session 9
    //****************************************************
    // Question 9_1
    q9_1.text = "If you were a villain, which would you most be like?";
    q9_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/9/question/1";
    q9_1.a = "Lex Luthor";
    q9_1.b = "Dr. Evil (or Ernst Starvo Blufeld, the character he is based on)";
    q9_1.c = "Agent Smith";
    q9_1.d = "Hans Gruber";
    q9_1.e = "HAL 9000";

    // Question 9_2
    q9_2.text = "As a secret agent meeting their bitter end, what would be your last words?";
    q9_2.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/9/question/2";
    q9_2.a = "Oh, good. For a moment there I thought we were in trouble. — Butch Cassidy";
    q9_2.b = "What’s that smell? — Tyler Durden, Fight Club";
    q9_2.c = "If you strike me down, I shall become more powerful than you can possibly imagine. — Obi-Wan Kenobi";
    q9_2.d = "Clever Girl! — Robert Muldoon, Jurassic Park";
    q9_2.e = "So we're all going to pull together and find a way to get out of here! — Russell Franklin, Deep Blue Sea";

    // Session 11
    //****************************************************
    // Question 11_1
    q11_1.text = "Which was your favorite product or presentation you saw over these two days?";
    q11_1.id = "tag:adlnet.gov,2013:expapi:1.0.0:xapiplugfest:session/11/question/1";

}


// *********************************************************************
// test data
// *********************************************************************
function populateTestData()
{
    // q 1-1
    var q1stmt = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_1.id},
                "result" : {"response" : "A"}};            
    var q1stmt2 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_1.id},
                "result" : {"response" : "A"}
                };
    var q1stmt3 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_1.id},
                "result" : {"response" : "B"}
                };
    var q1stmt4 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_1.id},
                "result" : {"response" : "A"}};            
    var q1stmt5 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_1.id},
                "result" : {"response" : "A"}
                };
    var q1stmt6 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_1.id},
                "result" : {"response" : "B"}
                };

    var q1stmts = [q1stmt, q1stmt2, q1stmt3, q1stmt4, q1stmt5, q1stmt6 ];
    var x = ADL.XAPIWrapper.sendStatements(q1stmts);

    // q 1-2
   // var stmt = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
   //             "verb" : ADL.verbs.answered,
   //             "object" : {"id" : q1_2.id},
   //             "result" : {"response" : "A"}};            
    var stmt2 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_2.id},
                "result" : {"response" : "A"}
                };
    var stmt3 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_2.id},
                "result" : {"response" : "B"}
                };
    var stmt4 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_2.id},
                "result" : {"response" : "C"}
                };
    var stmt5 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_2.id},
                "result" : {"response" : "C"}
                };
    var stmt6 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_2.id},
                "result" : {"response" : "D"}
                };

    // q 1-3
    var stmt7 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_3.id},
                "result" : {"response" : "1"}};            
    var stmt8 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_3.id},
                "result" : {"response" : "1"}
                };
    var stmt9 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_3.id},
                "result" : {"response" : "1"}
                };
    var stmt10 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_3.id},
                "result" : {"response" : "1"}
                };
    var stmt11 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_3.id},
                "result" : {"response" : "0"}
                };
    var stmt12 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_3.id},
                "result" : {"response" : "0"}
                };

    // q 1-4
    var stmt13 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_4.id},
                "result" : {"response" : "A"}};            
    var stmt14 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_4.id},
                "result" : {"response" : "B"}
                };
    var stmt15 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_4.id},
                "result" : {"response" : "C"}
                };
    var stmt16 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_4.id},
                "result" : {"response" : "D"}
                };
    var stmt17 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_4.id},
                "result" : {"response" : "D"}
                };
    var stmt18 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q1_4.id},
                "result" : {"response" : "B"}
                };


    // SECOND DAY **************************************************************************
    // q 7-1
    var stmt19 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_1.id},
                "result" : {"response" : "A"}};            
    var stmt20 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_1.id},
                "result" : {"response" : "B"}
                };
    var stmt21 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_1.id},
                "result" : {"response" : "B"}
                };
    var stmt22 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_1.id},
                "result" : {"response" : "B"}};            
    var stmt23 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_1.id},
                "result" : {"response" : "A"}
                };
    var stmt24 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_1.id},
                "result" : {"response" : "B"}
                };

    // q 7-2
    var stmt25 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_2.id},
                "result" : {"response" : "A"}};            
    var stmt26 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_2.id},
                "result" : {"response" : "A"}
                };
    var stmt27 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_2.id},
                "result" : {"response" : "B"}
                };
    var stmt28 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_2.id},
                "result" : {"response" : "B"}
                };
    var stmt29 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_2.id},
                "result" : {"response" : "C"}
                };
    var stmt30 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_2.id},
                "result" : {"response" : "D"}
                };

    // q 7-3
    var stmt31 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_3.id},
                "result" : {"response" : "0"}};            
    var stmt32 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_3.id},
                "result" : {"response" : "1"}
                };
    var stmt33 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_3.id},
                "result" : {"response" : "1"}
                };
    var stmt34 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_3.id},
                "result" : {"response" : "1"}
                };
    var stmt35 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_3.id},
                "result" : {"response" : "0"}
                };
    var stmt36 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_3.id},
                "result" : {"response" : "0"}
                };

    // q 7-4
    var stmt37 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_4.id},
                "result" : {"response" : "A"}};            
    var stmt38 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_4.id},
                "result" : {"response" : "B"}
                };
    var stmt39 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_4.id},
                "result" : {"response" : "C"}
                };
    var stmt40 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_4.id},
                "result" : {"response" : "D"}
                };
    var stmt41 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_4.id},
                "result" : {"response" : "D"}
                };
    var stmt42 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.answered,
                "object" : {"id" : q7_4.id},
                "result" : {"response" : "A"}
                };


    var stmts = [stmt2, stmt3, stmt4, stmt5, stmt6, stmt7, stmt8, stmt9, stmt10, stmt11, stmt12, stmt13, stmt14, stmt15, stmt16, stmt17, stmt18, stmt19, stmt20, stmt21, stmt22, stmt23, stmt24, stmt25, stmt26, stmt27, stmt28, stmt29, stmt30, stmt31, stmt32, stmt33, stmt34, stmt35, stmt36, stmt37, stmt38, stmt39, stmt40, stmt41, stmt42];
        var r = ADL.XAPIWrapper.sendStatements(stmts);

}


// test data
function populateTestRegistrations()
{
    
    var stmt = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Tom"}},
                "verb" : ADL.verbs.registered,
                "object" : {"id" : q0.id},
                "result" : {"response" : "A"}
                };            
    var stmt2 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jono"}},
                "verb" : ADL.verbs.registered,
                "object" : {"id" : q0.id},
                "result" : {"response" : "B"}
                };
    var stmt3 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Hank Hill"}},
                "verb" : ADL.verbs.registered,
                "object" : {"id" : q0.id},
                "result" : {"response" : "D"}
                };
    var stmt4 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Gordon Ramsey"}},
                "verb" : ADL.verbs.registered,
                "object" : {"id" : q0.id},
                "result" : {"response" : "C"}
                };
    var stmt5 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Stevie Wonder"}},
                "verb" : ADL.verbs.registered,
                "object" : {"id" : q0.id},
                "result" : {"response" : "A"}
                };
    var stmt6 = {"actor" : {"account" : { "homePage" : "http://example.com", "name" : "Jerry Garcia"}},
                "verb" : ADL.verbs.registered,
                "object" : {"id" : q0.id},
                "result" : {"response" : "C"}
                };
 
    var stmts = [stmt, stmt2, stmt3, stmt4, stmt5, stmt6];
    var r = ADL.XAPIWrapper.sendStatements(stmts);

}
